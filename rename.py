import couchdb
import re
import string
import time
import numpy
import json
import sys
import matplotlib.path as mplPath

def get_sa_code(coordinate):

    for item in sa_boundary_coordinates:
        if item.has_key('geometry') and item['geometry'] is not None:
            if item['geometry']['type'] == 'Polygon':
                polygon = item['geometry']['coordinates'][0]
                path = mplPath.Path(numpy.array(polygon))
                point = coordinate[0], coordinate[1]
                if path.contains_point(point):
                    return item['properties']['SA2_MAIN11']
            else:
                polygons = item['geometry']['coordinates']
                for polygon in polygons:
                    path = mplPath.Path(numpy.array(polygon[0]))
                    point = coordinate[0], coordinate[1]
                    if path.contains_point(point):
                        return item['properties']['SA2_MAIN11']

    return "-1"

def get_lga_code(coordinate):

    for item in lga_boundary_coordinates:
        if item.has_key('geometry') and item['geometry'] is not None:
            if item['geometry']['type'] == 'Polygon':
                polygon = item['geometry']['coordinates'][0]
                path = mplPath.Path(numpy.array(polygon))
                point = coordinate[0], coordinate[1]
                if path.contains_point(point):
                    return item['properties']['LGA_CODE11']
            else:
                polygons = item['geometry']['coordinates']
                for polygon in polygons:
                    path = mplPath.Path(numpy.array(polygon[0]))
                    point = coordinate[0], coordinate[1]
                    if path.contains_point(point):
                        return item['properties']['LGA_CODE11']

    return "-1"



with open('victoria_lga.json') as file:
    lga_boundary_coordinates = json.load(file)

with open('victoria_sa2.json') as file:
    sa_boundary_coordinates = json.load(file)

server = couchdb.Server('http://130.56.249.106:8080/')
sourcedb = server['sa_lga_tweets_db']

for id in sourcedb:

	tweet = sourcedb[id]
	
	if(tweet['lga_code'] == None or tweet['lga_code'] == -1):
		tweet['lga_code'] = int(get_lga_code(tweet['geometry']['coordinates']))

	if(tweet['sa_code'] == None or tweet['sa_code'] == -1):
		tweet['sa_code'] = int(get_sa_code(tweet['geometry']['coordinates']))

	print tweet
	print ""

	sourcedb.save(tweet)


